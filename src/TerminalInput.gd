class_name TerminalInput
extends TextEdit

func _process(_delta):
    grab_focus()
    
    if Input.is_action_just_pressed("enter"):
        _enter_command()
        
    _check_reset_text()
    _check_trim_text()
    _check_selected_column()

func _check_selected_column():
    if cursor_get_column() < 3:
        cursor_set_column(3)

func _check_reset_text():
    if text.length() < 3 or "\n" in text or not " > " in text:
        _reset_text()

func _reset_text():
    text = " > "
    cursor_set_column(3)

func _check_trim_text():
    if text.length() > 20:
        text = text.substr(0, 20)
        cursor_set_column(20)
        
func _enter_command():
    var command = text.trim_suffix("\n")
    command = command.substr(3, command.length() - 3)
    _main_node().enter_command(command)
    _reset_text()
    
func _main_node() -> MainNode:
    return get_node("/root/Main") as MainNode
