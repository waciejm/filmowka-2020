class_name MainNode
extends Control

enum { ROOM1, ROOM2 }

enum Command {
    UNKNOWN,
    CHANGE_ROOM,
    CHECK_FRIDGE,
    TAKE_KOMBUCHA,
    DRINK_KOMBUCHA_FAIL,
    LOOK_AT_PAINTING,
    TALK,
    DRINK_KOMBUCHA,
}

var room: int = ROOM1
var has_kombucha: bool = false
var talked: bool = false
var kombucha_level: int = 3

func _ready():
    _enter_room(ROOM1)

func _process(_delta):
    $Rooms/Room1/Player.hide()
    $Rooms/Room1/Player0.hide()
    $Rooms/Room1/Player1.hide()
    $Rooms/Room1/Player2.hide()
    $Rooms/Room1/Player3.hide()
    $Rooms/Room2/Player.hide()
    $Rooms/Room2/Player0.hide()
    $Rooms/Room2/Player1.hide()
    $Rooms/Room2/Player2.hide()
    $Rooms/Room2/Player3.hide()
    if not has_kombucha:
        $Rooms/Room1/Player.show()
        $Rooms/Room2/Player.show()
    elif kombucha_level == 3:
        $Rooms/Room1/Player3.show()
        $Rooms/Room2/Player3.show()
    elif kombucha_level == 2:
        $Rooms/Room1/Player2.show()
        $Rooms/Room2/Player2.show()
    elif kombucha_level == 1:
        $Rooms/Room1/Player1.show()
        $Rooms/Room2/Player1.show()
    elif kombucha_level == 0:
        $Rooms/Room1/Player0.show()
        $Rooms/Room2/Player0.show()
    
func _enter_room(new_room: int):
    match new_room:
        ROOM1:
            $Rooms/Room1.show()
            $Rooms/Room2.hide()
            $Music.volume_db = -12
        ROOM2:
            $Rooms/Room1.hide()
            $Rooms/Room2.show()
            $Music.volume_db = 0
    room = new_room

func _end_it():
    $EndItTimer.start(5)
    $Music.volume_db = 6
    $ViewportContainer.material = load("res://src/funky_shader.tres")

func _on_EndItTimer_timeout():
    get_tree().quit()
    
func enter_command(command: String):
    match parse_command(command):
        Command.UNKNOWN:
            $TerminalOutput.text = "aha?"
        Command.CHANGE_ROOM:
            if room == ROOM1:
                _enter_room(ROOM2)
                $TerminalOutput.text = "you enter the party room"
            else:
                _enter_room(ROOM1)
                $TerminalOutput.text = "you leave the party room"
        Command.CHECK_FRIDGE:
            $TerminalOutput.text = "two bottles of kombucha"
        Command.TAKE_KOMBUCHA:
            has_kombucha = true
            $TerminalOutput.text = "you take the kombucha"
        Command.TALK:
            if not talked:
                $TerminalOutput.text = "nothing is real man"
            else:
                $TerminalOutput.text = "man I want some kombucha"
            talked = true
        Command.DRINK_KOMBUCHA:
            kombucha_level -= 1
            if kombucha_level == 2:
                $TerminalOutput.text = "you start to understand..."
            elif kombucha_level == 1:
                $TerminalOutput.text = "you know you can break free..."
            elif kombucha_level == 0:
                $TerminalOutput.text = "maybe this was a mistake..."
                _end_it()
            
        Command.DRINK_KOMBUCHA_FAIL:
            $TerminalOutput.text = "you can't drink kombucha alone!"
        Command.LOOK_AT_PAINTING:
            $TerminalOutput.text = "the painting looks back..."
            

func parse_command(command: String) -> int:
    if (("leave" in command or "go" in command or "walk" in command)
    and ("room" in command or "door" in command)):
        return Command.CHANGE_ROOM
    elif room == ROOM1 and has_kombucha and "drink" in command:
        return Command.DRINK_KOMBUCHA_FAIL
    elif room == ROOM1 and "fridge" in command:
        return Command.CHECK_FRIDGE
    elif room == ROOM1 and "kombucha" in command:
        return Command.TAKE_KOMBUCHA
    elif room == ROOM1 and ("painting" in command or "picture" in command):
        return Command.LOOK_AT_PAINTING
    elif room == ROOM2 and "talk" in command:
        return Command.TALK
    elif room == ROOM2 and has_kombucha and "drink" in command:
        return Command.DRINK_KOMBUCHA
    else:
        return Command.UNKNOWN
